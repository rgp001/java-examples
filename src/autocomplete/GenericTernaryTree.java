package autocomplete;

import javax.swing.text.html.Option;
import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;


public class GenericTernaryTree<T extends Comparable<T>> {

    private Node root = null;
    private boolean caseSensitive = true;

    private class Node {

        T data;
        boolean end;

        int weight = 0;

        Node middle = null;
        Node left = null;
        Node right = null;

        public Node(T data) {
            super();
            this.data = data;
        }

    }

    public GenericTernaryTree() {
        super();
    }

    public GenericTernaryTree(boolean caseSensitive) {
        super();
        this.caseSensitive = caseSensitive;
    }


    public void insert(T[] array) {
        root = insert(array, 0, root);
    }

    private Node insert(T[] parts, int i, Node ptr) {

        if (i < parts.length) {

            Node temp = null;

            if (ptr == null) {
                temp = new Node(parts[i]);
                ptr = temp;
            }

            if (caseSensitive || ! (parts[i] instanceof Character)) {

                if (parts[i].compareTo(ptr.data) > 0) {
                    ptr.right = insert(parts, i, ptr.right);
                } else if (parts[i].compareTo(ptr.data) < 0) {
                    ptr.left = insert(parts, i, ptr.left);
                } else {
                    if (++i < parts.length)
                        ptr.middle = insert(parts, i, ptr.middle);
                    else
                        ptr.end = true;
                }
            } else {

                Character data = (Character) ptr.data;
                Character sample = (Character) parts[i];

                data = Character.toLowerCase(data);
                sample = Character.toLowerCase(sample);

                if (sample.compareTo(data) > 0) {
                    ptr.right = insert(parts, i, ptr.right);
                } else if (sample.compareTo(data) < 0) {
                    ptr.left = insert(parts, i, ptr.left);
                } else {
                    if (++i < parts.length)
                        ptr.middle = insert(parts, i, ptr.middle);
                    else
                        ptr.end = true;
                }
            }



        }

        return ptr;

    }

    public Map<T[], Integer> traverse(int limit) {

        Map<T[], Integer> map = new HashMap<>();
        List<T> matchFormer = new LinkedList<>();
        traverse(root, matchFormer, map, false, limit);
        return map;
    }

    public void traverse(Node ptr, List<T> matchFormer, Map<T[], Integer> weightMap,
                         boolean addLeafOnly, int limit) {

        if (ptr.left != null) {
            List<T> newStr = new LinkedList<>(matchFormer);
            traverse(ptr.left, newStr, weightMap, addLeafOnly, limit);
        }

        if (ptr.right != null) {
            List<T> newStr = new LinkedList<>(matchFormer);
            traverse(ptr.right, newStr, weightMap, addLeafOnly, limit);
        }

        matchFormer.add(ptr.data);

        if (ptr.end && !addLeafOnly) {

            if (matchFormer.size() > 0) {
                T [] arr = (T[]) Array.newInstance(matchFormer.get(0).getClass(), matchFormer.size());
                arr = matchFormer.toArray(arr);

                if (weightMap.size() < limit)
                    weightMap.put(arr, ptr.weight);
                else {
                   Optional<Map.Entry<T[], Integer>> entry = weightMap.entrySet().stream().filter(e -> e.getValue() < ptr.weight).min((e1, e2) -> e1.getValue() - e2.getValue());
                   if (entry.isPresent()) {
                       T[] t = entry.get().getKey();
                       weightMap.remove(t);
                       weightMap.put(arr, ptr.weight);
                   }

                }
            }
        }

        if (ptr.middle != null) {
            List<T> newStr = new LinkedList<>(matchFormer);
            traverse(ptr.middle, newStr, weightMap, addLeafOnly, limit);
        }

    }


    public void select(T[] selection) {
        Node ptr = root;
        int i = 0;

        while (ptr != null && i <= selection.length -1) {
            int diff = ptr.data.compareTo(selection[i]);

            if (diff == 0) {
                if (i == selection.length -1)
                    ptr.weight++;
                ptr = ptr.middle;
                ++i;
            } else if (diff > 0){
                ptr = ptr.left;
            } else {
                ptr = ptr.right;
            }

        }

    }

    public List<T[]> getMatches(T[] match, int limit) {

        Map<T[], Integer> matched = new HashMap<>();

        Node ptr = root;

        int i = 0;

        List<T> matchFormer = new LinkedList<>();

        while (ptr != null && i <= match.length -1 ) {

            int diff = ptr.data.compareTo(match[i]);


            if (diff == 0) {

                matchFormer.add(ptr.data);

                if (ptr.end) {

                    T [] arr = (T[]) Array.newInstance(matchFormer.get(0).getClass(), matchFormer.size());
                    arr = matchFormer.toArray(arr);

                    if (Arrays.equals(match, arr)) {
                        matched.put(arr, ptr.weight);
                    }
                }

                ptr = ptr.middle;
                ++i;
            } else if (diff > 0){
                ptr = ptr.left;
            } else {
                ptr = ptr.right;
            }
        }

        if (ptr != null) {
            traverse(ptr, matchFormer, matched, false, limit);
        }

        return matched.entrySet().stream().sorted((e1, e2) -> e2.getValue() - e1.getValue()).map(Map.Entry::getKey).collect(Collectors.toList());
    }


}
