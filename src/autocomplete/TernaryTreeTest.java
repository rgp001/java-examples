package autocomplete;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class TernaryTreeTest {

    public static void main (String [] args) throws IOException {
       StringCharTree tree = new StringCharTree();

       try (BufferedReader br = new BufferedReader(new FileReader("/home/guruprasad/Downloads/words.txt"))) {

           String word;
           while ((word = br.readLine()) != null)
            tree.insert(word);
       }

        tree.select("sunset");
        tree.select("sunrise");
        tree.select("sunny");
        tree.select("sunny");

       List<String> matches = tree.getMatches("su", 5);
       matches.stream().forEach(System.out::println);

    }
}
