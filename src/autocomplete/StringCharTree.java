package autocomplete;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringCharTree extends GenericTernaryTree<Character> {

    private static Character[] getCharacterArray(String str) {
        char [] chararr = str.toCharArray();
        Character [] objectarr = new Character[chararr.length];
        for (int i = 0;  i < chararr.length; i++) {
            objectarr[i] = Character.toLowerCase(chararr[i]);
        }
        return objectarr;
    }

    public void insert(String str) {
        insert(getCharacterArray(str));
    }

    private String getString(Character[] chars) {
        StringBuilder str = new StringBuilder();
        Arrays.stream(chars).forEach(str::append);
        return str.toString();
    }

    public List<String> getMatches(String str,int limit) {
        return getMatches(getCharacterArray(str), limit).stream().map(this::getString).collect(Collectors.toList());
    }

    public void select(String selected){
        select(getCharacterArray(selected));
    }
}
