package puzzle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

public class PuzzleModel implements Cloneable {

    public int grid [][];
    int n;
    private int blank_pos;

    private ModelListener listener;

    public ModelListener getListener() {
        return listener;
    }

    public void setListener(ModelListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PuzzleModel that = (PuzzleModel) o;
        return n == that.n &&
                blank_pos == that.blank_pos &&
                Arrays.deepEquals(grid, that.grid);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(n, blank_pos);
        result = 31 * result + Arrays.deepHashCode(grid);
        return result;
    }

    private PuzzleModel() {

    }

    public PuzzleModel(int n) {
        this.n = n;
        this.blank_pos = n*n;

        grid = new int[n][n];

        for (int i = 1; i <= n*n; i++) {
            grid[(i-1)/n][(i-1)%n] = i;
        }
    }

    public int getByIndex(int i) {
        return grid[(i-1)/n][(i-1)%n];
    }

    @Override
    public PuzzleModel clone() {
        PuzzleModel clone = new PuzzleModel();
        clone.n = n;
        clone.blank_pos = blank_pos;

        int[][] copy = Arrays.stream(grid).map(int[]::clone).toArray(int[][]::new);
        clone.grid = copy;

        return clone;
    }

    public List<PuzzleModel> getPossibleMoves() {

        List<PuzzleModel> moves = new ArrayList<>();

        int y = (blank_pos -1)/n;
        int x = (blank_pos -1)%n;

        // swap blank with left
        if (x > 0 ) {
            PuzzleModel left = clone();
            int temp = left.grid[y][x-1];
            left.grid[y][x-1] = left.grid[y][x];
            left.grid[y][x] = temp;
            left.blank_pos = blank_pos -1;

            moves.add(left);
        }

        //swap blank with right
        if (x < n-1) {
            PuzzleModel right = clone();
            int temp = right.grid[y][x+1];
            right.grid[y][x+1] = right.grid[y][x];
            right.grid[y][x] = temp;
            right.blank_pos = blank_pos + 1;

            moves.add(right);
        }

        // swap blank with up
        if (y > 0 ) {
            PuzzleModel up = clone();
            int temp = up.grid[y-1][x];
            up.grid[y-1][x] = up.grid[y][x];
            up.grid[y][x] = temp;
            up.blank_pos = blank_pos -n;

            moves.add(up);
        }

        //swap blank with down
        if (y < n-1) {
            PuzzleModel down = clone();
            int temp = down.grid[y+1][x];
            down.grid[y+1][x] = down.grid[y][x];
            down.grid[y][x] = temp;
            down.blank_pos = blank_pos + n;

            moves.add(down);
        }
        return moves;
    }

    //index goes from 1-n*n
    public void moveIfPossible(int index) {
        int srcY= (index -1)/n;
        int srcX = (index-1) %n;

        int destY = (blank_pos-1)/n;
        int destX = (blank_pos-1)%n;

        if (destY == srcY) {
            if (srcX - destX == 1) {
                blank_pos++;

                grid[destY][destX] = grid[srcY][srcX];
                grid[srcY][srcX] = n*n;

                listener.modelChanged();
            } else if (destX - srcX == 1) {
                blank_pos--;

                grid[destY][destX] = grid[srcY][srcX];
                grid[srcY][srcX] = n*n;

                listener.modelChanged();
            }
        } else if (destX == srcX) {

            if (srcY - destY == 1) {
                blank_pos+=n;

                grid[destY][destX] = grid[srcY][srcX];
                grid[srcY][srcX] = n*n;

                listener.modelChanged();
            } else if (destY - srcY == 1) {
                blank_pos-=n;

                grid[destY][destX] = grid[srcY][srcX];
                grid[srcY][srcX] = n*n;

                listener.modelChanged();
            }
        }
    }

    public void printGrid() {

        Arrays.stream(grid).forEach(arr -> {
            System.out.println(Arrays.stream(arr).mapToObj(i -> String.valueOf(i)).reduce((x,y) -> x + " " + y).get());
        });

    }

    public void shuffle() {

        PuzzleModel move = this;

        for (int i = 1; i<=100; i++) {
            List<PuzzleModel> moves = move.getPossibleMoves();

            int randomMoveIndex = randomWithRange(0, moves.size()-1);
            move = moves.get(randomMoveIndex);
        }

        this.grid = move.grid;
        this.blank_pos = move.blank_pos;

        listener.modelChanged();

    }

    private int randomWithRange(int min, int max){   //defining method for a random number generator

        int range = (max - min) + 1;
        return (int)(Math.random() * range) + min;
    }
}
