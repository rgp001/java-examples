package puzzle;

import javax.swing.*;
import java.awt.*;

public class CustomButton extends JButton {

    private int index;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public CustomButton(int index, String label) {
        super(label);
        this.index = index;
        setFont(new Font("Arial", Font.BOLD, 30));
    }
}
