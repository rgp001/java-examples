package puzzle;

import java.awt.*;
import javax.swing.*;

public class SquarePuzzle{
    JFrame frame;
    CustomButton [] button;

    JMenuBar menuBar = new JMenuBar();
    JMenu actionMenu = new JMenu("Actions");
    JMenuItem restartItem = new JMenuItem("Restart");
    JMenuItem shuffleItem = new JMenuItem("Shuffle");
    JMenuItem solveItem = new JMenuItem("Solve");

    JPanel panel = new JPanel();

    JPanel nextPanel = new JPanel();
    JButton nextButton = new JButton("Next");


    public SquarePuzzle(PuzzleModel model, Controller controller) {

        int n = model.n;

        frame=new JFrame();


        shuffleItem.addActionListener(controller);
        solveItem.addActionListener(controller);

        menuBar.add(actionMenu);

        actionMenu.add(restartItem);
        actionMenu.add(shuffleItem);
        actionMenu.add(solveItem);

        frame.setJMenuBar(menuBar);

        frame.add(panel, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        button = new CustomButton[n*n];

        for (int i = 1; i <= n*n; i++) {

            int val = model.getByIndex(i);

            String label = val == n*n ? "" : String.valueOf(val);

            button[i-1] = new CustomButton(i,label);
            button[i-1].addActionListener(controller);
            panel.add(button[i-1]);
        }

        panel.setLayout(new GridLayout(n, n));

        frame.add(nextPanel, BorderLayout.SOUTH);
        nextPanel.add(nextButton);

        nextButton.addActionListener(controller);
        //setting grid layout of 3 rows and 3 columns

        frame.setSize(500,500);
        frame.setVisible(true);
    }

    public void renderPuzzle(PuzzleModel model) {

        int n = model.n;

        for (int i = 1; i <= n*n; i++) {

            int val = model.getByIndex(i);
            String label = val == n*n ? "" : String.valueOf(val);
            button[i-1].setText(label);

        }
    }



}
