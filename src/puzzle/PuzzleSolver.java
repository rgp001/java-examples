package puzzle;

import java.awt.desktop.SystemEventListener;
import java.util.*;

public class PuzzleSolver {

    private PuzzleModel src;
    private PuzzleModel destination ;

    public static class PuzzlePath {

        PuzzleModel curr;
        PuzzlePath prev;

        int manhattanDistance;
    }


    private static int calculateManhattanDistance(PuzzleModel model) {

        int d = 0;

        for (int row = 0; row < model.n; row++) {
            for (int col = 0; col < model.n; col++) {
                int num = model.grid[row][col];

                int expected_row = (num-1)/model.n;
                int expected_col = (num-1) % model.n;

                d += Math.abs(expected_row-row)+Math.abs(expected_col-col);
            }
        }

        return d;
    }

    public PuzzleSolver(int n) {
        destination = new PuzzleModel(n);
    }

    public Stack<PuzzleModel> solve(PuzzleModel src) {
        this.src = src;

        PriorityQueue<PuzzlePath> pQueue = new PriorityQueue<PuzzlePath>((p1, p2) -> {
            return p1.manhattanDistance - p2.manhattanDistance;
        });

        Set<PuzzleModel> visited = new HashSet<>();

        Stack<PuzzleModel> path = new Stack<>();

        if (!src.equals(destination)) {

            PuzzleModel curr = src;

            PuzzlePath currPath = new PuzzlePath();
            currPath.curr = curr;
            int d = calculateManhattanDistance(curr);
            currPath.manhattanDistance = d;

            int iter_count = 0;

            visited.add(curr);

            pQueue.add(currPath);

            while (!curr.equals(destination)) {

                final PuzzlePath temp = currPath;

                List<PuzzleModel> moves = curr.getPossibleMoves();
                moves.stream().filter(o -> !visited.contains(o)).map(model -> {
                    PuzzlePath p = new PuzzlePath();
                    p.prev = temp;
                    p.curr = model;
                    int md = calculateManhattanDistance(model);
                    p.manhattanDistance = md;
                    visited.add(model);
                    return p;
                }).forEach(pQueue::add);

                currPath = pQueue.remove();
                iter_count++;
                System.out.println(iter_count);
                curr = currPath.curr;

            }

            System.out.println("Done with solution");

            while (currPath != null) {
                path.push(currPath.curr);
                currPath = currPath.prev;
            }

        }

        return path;

    }

}
