package puzzle;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Stack;

public class Controller implements ModelListener, ActionListener {

    private PuzzleModel model;
    private Stack<PuzzleModel> solution;

    private SquarePuzzle view;

    private static PuzzleSolver solver;


    public static void main(String[] args) {

        PuzzleModel model = new PuzzleModel(4);
        Controller controller = new Controller();
        controller.model = model;
        model.setListener(controller);

        SquarePuzzle view = new SquarePuzzle(model, controller);
        controller.view = view;

        solver = new PuzzleSolver(model.n);
    }


    @Override
    public void modelChanged() {
        view.renderPuzzle(model);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        if (actionEvent.getSource() instanceof CustomButton) {
            CustomButton button = (CustomButton) actionEvent.getSource();
            int index = button.getIndex();

            model.moveIfPossible(index);
        } else if (actionEvent.getSource() instanceof JMenuItem) {
            JMenuItem item = (JMenuItem) actionEvent.getSource();

            if (item.getText().equals("Shuffle")) {
                model.shuffle();
            } else if (item.getText().equals("Solve")) {
                solution = solver.solve(model);
            }
        } else if (actionEvent.getSource() instanceof JButton) {
            JButton button = (JButton) actionEvent.getSource();
            if (button.getText().equals("Next")) {
                PuzzleModel model = solution.pop();
                view.renderPuzzle(model);
            }
        }
    }
}
