package puzzle;

public interface ModelListener {

    void modelChanged();
}
