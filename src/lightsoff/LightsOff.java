package lightsoff;

import java.util.*;

public class LightsOff {

    protected  boolean[][] grid;
    protected  boolean[][] target;

    private int d;

    private Set<Integer> solutionSpace = new TreeSet<>();

    public LightsOff(int dimension, boolean[][] target) {

        d = dimension;
        this.target = target;
        grid = new boolean[dimension][dimension];

        for (boolean [] arr : grid) {
            Arrays.fill(arr, true);
        }
    }

    public LightsOff(int dimension) {
        this(dimension, new boolean[dimension][dimension]);
        for (boolean [] arr : target) {
            Arrays.fill(arr, false);
        }
    }

    private int getIndexForRowAndCol(int row, int col) {
        return row*d + col;
    }

    private Set<Integer> getRelyingPositions(int row, int col) {
        Set<Integer> positions = new HashSet<>();

        if (row > 0) {
            positions.add(getIndexForRowAndCol(row-1, col));
            if (row  ==  d -1 && col > 0) {
              positions.add(getIndexForRowAndCol(row, col -1));
            }
        }

        return  positions;
    }

    private Set<Integer> getAdjacentPositions(int row, int col) {
        Set<Integer> adjacent = new HashSet<>();

        if (row > 0) {
            adjacent.add(getIndexForRowAndCol(row-1, col));
        }

        if (row < d-1) {
            adjacent.add(getIndexForRowAndCol(row+1, col));
        }

        if (col > 0) {
            adjacent.add(getIndexForRowAndCol(row, col-1));
        }

        if (col < d-1) {
            adjacent.add(getIndexForRowAndCol(row, col+1));
        }


        return adjacent;
    }

    private boolean inCorrectState(int pos) {
        int r = pos / d;
        int c = pos % d;

        return grid[r][c] == target[r][c];
    }

    private void toggle(Set<Integer> positions) {
        for (int pos : positions) {
            int r = pos / d;
            int c = pos % d;
            grid[r][c]=!grid[r][c];
        }
    }

    private boolean solve(int k) {

        if( k == d*d) {
            return Arrays.deepEquals(grid, target);
        }

        int row = k / d;
        int col = k % d;

        // get those positions which rely on this one to correct them. This will be the immediate upper row and left
        Set<Integer> relyingPositions = getRelyingPositions(row, col);

        if (!relyingPositions.isEmpty()) {

            if (relyingPositions.stream().allMatch(this::inCorrectState)) {
                return solve(k+1);
            } else if (relyingPositions.stream().noneMatch(this::inCorrectState)) {

                Set<Integer> togglePositions = getAdjacentPositions(row, col);
                togglePositions.add(k);
                toggle(togglePositions);

                solutionSpace.add(k);

                boolean canSolve = solve(k+1);

                if (canSolve) {
                    return true;
                } else {
                    //rollback
                    solutionSpace.remove(k);
                    toggle(togglePositions);

                    return false;
                }

            } else {
               return false;
            }
        } else {
            // already in target position solve for k+1
            if (grid[row][col] == target[row][col]) {
                boolean canSolve = solve(k+1);

                if (canSolve) {
                    return true;
                } else {
                    Set<Integer> togglePositions = getAdjacentPositions(row, col);
                    togglePositions.add(k);
                    toggle(togglePositions);

                    solutionSpace.add(k);

                    return solve(k+1);

                }
            } else {

                Set<Integer> togglePositions = getAdjacentPositions(row, col);
                togglePositions.add(k);
                toggle(togglePositions);

                solutionSpace.add(k);

                boolean canSolve = solve(k+1);

                if (canSolve) {
                    return true;
                } else {
                    // rollback and solve for k+1
                    solutionSpace.remove(k);
                    toggle(togglePositions);

                    return solve(k+1);

                }
            }
        }

    }


    public static void main (String [] args) {

        Scanner myInput = new Scanner( System.in );
        System.out.print( "Enter the Dimension of the light grid : " );
        int d = myInput.nextInt();

        LightsOff problem = new LightsOff(d);
        problem.solve(0);

        problem.solutionSpace.stream().forEach(p -> System.out.print(p+1 + " "));
    }
}
