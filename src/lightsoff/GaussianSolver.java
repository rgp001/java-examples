package lightsoff;

import java.util.*;
import java.util.function.Function;

public class GaussianSolver {

    private static Function<Integer , BitSet[]> formEquation = n -> {

        BitSet[] eq = new BitSet[n*n];

        for (int i = 0; i < n*n; i++) {
            eq[i] = new BitSet(n*n + 1);
            eq[i].set(n*n);
            eq[i].set(i);
            getAdjacentPositions(i/n, i%n, n).stream().forEach(eq[i]::set);
        }

        return eq;
    };

    private static Function<BitSet[], BitSet[]> reOrder = matrix -> {

        int dim  = (int) Math.sqrt(matrix.length);
        int dim2 = matrix.length;

        for (int i = 1; i < dim2 - dim; i++ ) {

            BitSet temp = matrix[i];
            matrix[i] = matrix[i+dim];
            matrix[i+dim] = temp;
        }

        return matrix;
    };

    private static Function<BitSet[], BitSet[]> triangulate = matrix -> {

        int dim  = (int) Math.sqrt(matrix.length);
        int dim2 = matrix.length;

        for (int i = dim2-dim; i < dim2; i++) {
            int pos = matrix[i].nextSetBit(0);

            while (pos != -1 && pos < i) {
                matrix[i].xor(matrix[pos]);
                pos = matrix[i].nextSetBit(pos);

                if (pos > i) {
                    //if (!matrix[i].get(i)){
                        BitSet temp = matrix[i];
                        matrix[i] = matrix[pos];
                        matrix[pos] = temp;
                    pos = matrix[i].nextSetBit(0);
                    //}
                }
            }
        }

        return matrix;
    };

    private static Function<Integer, BitSet[]> rowEchelon = formEquation.andThen(reOrder).andThen(triangulate);

    private static int getIndexForRowAndCol(int row, int col, int d) {
        return row*d + col;
    }

    private static Set<Integer> getAdjacentPositions(int row, int col, int d) {
        Set<Integer> adjacent = new HashSet<>();

        if (row > 0) {
            adjacent.add(getIndexForRowAndCol(row-1, col, d));
        }

        if (row < d-1) {
            adjacent.add(getIndexForRowAndCol(row+1, col, d));
        }

        if (col > 0) {
            adjacent.add(getIndexForRowAndCol(row, col-1, d));
        }

        if (col < d-1) {
            adjacent.add(getIndexForRowAndCol(row, col+1, d));
        }


        return adjacent;
    }

    private static String printBitSet(BitSet bitSet, int d) {

        StringBuilder s = new StringBuilder();

        for( int i = 0; i < bitSet.length();  i++ )
        {
            s.append( bitSet.get( i ) == true ? 1: 0 );
            s.append(" ");
        }

        if (bitSet.length() < d*d + 1 ) {
            for (int i = bitSet.length() + 1; i <= d*d +1; i++) {
                s.append(0);
                s.append(' ');
            }
        }
        return s.toString();
    }

    private static List<BitSet> backSubstitute(BitSet[] eq, BitSet values, int i) {

        if (i == -1) {
            return List.of(values);
        }

        if (!eq[i].get(i)) {
            BitSet set = (BitSet) values.clone();
            set.set(i);

            BitSet unset = (BitSet) values.clone();

            List<BitSet> combined = new ArrayList<>();

            combined.addAll(backSubstitute(eq, set, i -1));
            combined.addAll(backSubstitute(eq, unset, i -1));

            return combined;
        } else {

            boolean value = eq[i].get(eq.length);

            for (int col = i+1; col < eq.length; col++) {
                value = value ^ (values.get(col) && eq[i].get(col));
            }

            if (value) {
                values.set(i);
            }
            return backSubstitute(eq, values, i -1);
        }

    }

    private static void print(BitSet[] bs, int d) {
        Arrays.stream(bs).map(b -> printBitSet(b, d)).forEach(System.out::println);
    }

    public  static void main (String [] args) {

        Scanner myInput = new Scanner( System.in );
        System.out.print( "Enter the Dimension of the light grid : " );
        int d = myInput.nextInt();
        BitSet[] eq = rowEchelon.apply(d);

        List<BitSet> solutions = backSubstitute(eq, new BitSet(d*d), d*d - 1);
        solutions.stream().forEach(System.out::println);

    }
}
