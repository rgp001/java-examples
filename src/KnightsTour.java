

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;


public class KnightsTour {
	
	int n;
	Set<Position> visited = new LinkedHashSet<>();
	
	public KnightsTour(int n) {
		this.n = n;
	}
	
	class Position {
		
		int x;
		int y;
		public Position(int x, int y) {
			super();
			this.x = x;
			this.y = y;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getEnclosingInstance().hashCode();
			result = prime * result + x;
			result = prime * result + y;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Position other = (Position) obj;
			if (!getEnclosingInstance().equals(other.getEnclosingInstance()))
				return false;
			if (x != other.x)
				return false;
			if (y != other.y)
				return false;
			return true;
		}
		private KnightsTour getEnclosingInstance() {
			return KnightsTour.this;
		}
		
		@Override
		public String toString() {
			return "Position x " +  x + " y " + y;
		}
		
	}
	
	
	private Position getMoveUpAndRight(Position pos) {
		return new Position(pos.x+1, pos.y+2);
	}
	
	
	private Position getMoveUpAndLeft(Position pos) {
		return new Position(pos.x-1,  pos.y+2);
	}
	
	private Position getMoveRightAndUp(Position pos) {
		return new Position(pos.x+2, pos.y+1);
	}
	
	private Position getMoveLeftAndUp(Position pos) {
		return new Position(pos.x-2, pos.y+1);
	}
 	
	private Position getMoveDownAndRight(Position pos) {
		return new Position(pos.x+1,pos.y-2);
	}
	
	
	private Position getMoveDownAndLeft(Position pos) {
		return new Position(pos.x-1, pos.y-2);
	}
	
	private Position getMoveRightAndDown(Position pos) {
		return new Position(pos.x+2, pos.y-1);
	}
	
	private Position getMoveLeftAndDown(Position pos) {
		return new Position(pos.x-2, pos.y-1);
	}
	
	private boolean isValidPosition(Position pos) {
		
		if (pos.x <= 0 || pos.x > n) {
			return false;
		}
		
		if (pos.y <= 0 || pos.y > n) {
			return false;
		}
		
		return true;
	}
	
	public List<Position> getValidAdjacentPositions(Position pos) {
		
		List<Position> validPositions = new ArrayList<KnightsTour.Position>();
		
		// Up movements
		
		Position upAndRight = getMoveUpAndRight(pos);
		if (isValidPosition(upAndRight)) {
			validPositions.add(upAndRight);
		}
		
		Position upAndLeft = getMoveUpAndLeft(pos);
		if (isValidPosition(upAndLeft)) {
			validPositions.add(upAndLeft);
		}
		
		Position rightAndUp = getMoveRightAndUp(pos);
		if (isValidPosition(rightAndUp)) {
			validPositions.add(rightAndUp);
		}
		
		Position leftAndUp = getMoveLeftAndUp(pos);
		if (isValidPosition(leftAndUp)) {
			validPositions.add(leftAndUp);
		}
		
		// Down movements
		
		Position downAndRight = getMoveDownAndRight(pos);
		if (isValidPosition(downAndRight)) {
			validPositions.add(downAndRight);
		}
		
		Position downAndLeft = getMoveDownAndLeft(pos);
		if (isValidPosition(downAndLeft)) {
			validPositions.add(downAndLeft);
		}
		
		Position rightAndDown = getMoveRightAndDown(pos);
		if (isValidPosition(rightAndDown)) {
			validPositions.add(rightAndDown);
		}
		
		Position leftAndDown = getMoveLeftAndDown(pos);
		if (isValidPosition(leftAndDown)) {
			validPositions.add(leftAndDown);
		}
		
		return validPositions.stream().filter((p) -> !visited.contains(p)).collect(Collectors.toList());
		
	}
	
	
	public boolean foundPath(Position current, List<Position> freedom) {
		
		visited.add(current);
		
		if (visited.size() < n*n) {
			
			List<Position> possibilities  = freedom.size() > 0 ? freedom : getValidAdjacentPositions(current);
			
			if (possibilities.isEmpty()) {
				visited.remove(current);
				return false;
			} else {
				
				Map<Position, List<Position>> map = new HashMap<>();
				
				for (Position possibilty : possibilities) {
					map.put(possibilty, getValidAdjacentPositions(possibilty));
				}
				
				
				
				while (!map.isEmpty()) {
					
					Entry<Position, List<Position>> posLeastDegree =  
							map.entrySet().stream().max((entry1, entry2) -> entry2.getValue().size() - entry1.getValue().size()).get();
					
					if (foundPath(posLeastDegree.getKey(), posLeastDegree.getValue())) {
						return true;
					}
					
					map.remove(posLeastDegree.getKey());
				}
				
				
				
				//--------------------------------
				//Iterator<Position> it = possibilities.iterator();
				
				/*while (it.hasNext()) {
					Position possibility = it.next();
					if (foundPath(possibility)) {
						return true;
					}
				}*/
				visited.remove(current);
				return false;
				
			}
		} else {
			return true;
		}
		
	}
	
	public void startTour() {
		Position start = new Position(4, 4);
		foundPath(start, new ArrayList<KnightsTour.Position>());
		System.out.println(visited.size());
		visited.stream().forEach(System.out::println);
	}
	
	public static void main (String [] args) {
		
		KnightsTour tour = new KnightsTour(8);
		tour.startTour();
		System.out.println("done");
	}

}
