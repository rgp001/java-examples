package src;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Permutations {
	
	
	public static int[][] computePermutations (int n) {
		
		int [] numbers = IntStream.range(1, n+1).toArray();	
		return computePermutations(numbers);
	
	}

	private static int[][] computePermutations(int[] numbers) {
		
		if (numbers.length == 1) {
			return new int[][] {{numbers[0]}};
		} else {
			
			int[][] permuations = new int[0][0];
			
			for (int i = 0 ; i < numbers.length; i++) {
				final int num_index = numbers[i];
				int [] temp = Arrays.stream(numbers).filter((num) -> num != num_index).toArray();
				
				int[][] permutationsAfterExclusion = computePermutations(temp);
				
				permuations = includeAndRotate(numbers[i], permutationsAfterExclusion);
				
				
			}
			return permuations;
		}
		
	}

	private static int[][] includeAndRotate(int num, int[][] permutationsAfterExclusion) {
		
		int numElements = permutationsAfterExclusion[0].length;
		int numPermutations = permutationsAfterExclusion.length;
		
		int [][] permutations = new int[numPermutations * (numElements+1)][numElements+1];
		
		int index = 0;
		
		for (int i = 0; i < permutationsAfterExclusion.length; i++) {
			
			int[] permutation = permutationsAfterExclusion[i];	
			
			for (int j = 0; j <= permutation.length; j++) {
				
				for (int k = 0; k <= permutation.length; k++) {
					
					if (k == j) {
						permutations[index][k] = num;
					} else if (k < j) {
						permutations[index][k] = permutation[k];
					} else {
						permutations[index][k] = permutation[k-1];
					}
					
				}
				index++;
			}
			
			
			
		}
		return permutations;
	}

	public static void main(String[] args) {
		
		int [][] permutations = computePermutations(10);
		
		Arrays.stream(permutations).forEach((arr)-> {
			System.out.println();
			Arrays.stream(arr).forEach(System.out::print);
		});
		

	}

}
