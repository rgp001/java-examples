package spellCheck;

import javax.swing.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static spellCheck.EditDistance.editDistance;

public class SpellChecker {

    private static Map<Integer, List<String>> wordLengthMap;
    private static final int TOLERANCE = 2;


    private static class Node {
        String word;

        public Node(String word) {
            this.word = word;
        }

        Map<Integer, Node> adjacentNodes = new TreeMap<>();
    }

    private static class Element {
        String word;
        int distance;

        public Element(String word, int distance) {
            this.word = word;
            this.distance = distance;
        }
    }

    private static Map<Integer, List<String>> group(List<String> words) {
        return words.stream().collect(Collectors.groupingBy(e -> e.length()));
    }

    private static Node insertWord(Node root, String word) {

        if (root == null) {
            root = new Node(word);
            return root;
        } else {
            Node ptr = root;
            String ptr_word = ptr.word;

            try {
                int distance = editDistance(ptr_word, word);

                if (!ptr.adjacentNodes.containsKey(distance)) {
                    Node child = new Node(word);
                    ptr.adjacentNodes.put(distance, child);
                } else {
                    Node child = ptr.adjacentNodes.get(distance);
                    insertWord(child, word);
                }
            } catch(Exception e) {
                System.out.println(ptr_word + " " + word);
                throw e;
            }


        }

        return root;
    }

    private static Integer minRange(int length) {
        return length - TOLERANCE <= 0 ? 1 : length - TOLERANCE;
    }

    private static Node formBkTree(String missSpelled) {

        int length = missSpelled.length();
        AtomicReference<Node> rootRef = new AtomicReference<>();

        wordLengthMap.computeIfPresent(length, (key, val) -> {

            Node root = insertWord(null, val.get(0));
            rootRef.set(root);
            val.subList(1, val.size()).stream().forEach(word -> insertWord(root, word));

            IntStream.rangeClosed(minRange(length), length + TOLERANCE).filter(x -> x != length).forEach(l -> {
                if (wordLengthMap.containsKey(l)) {
                    List<String> words = wordLengthMap.get(l);
                    words.stream().forEach(word -> insertWord(root, word));
                }
            });



            return  val;
        });

        return rootRef.get();
    }

    private static PriorityQueue<Element> getWordsWithinRange(Node root, String missSpelled) {

        var distance = editDistance(root.word, missSpelled);
        PriorityQueue<Element> words = new PriorityQueue<>((e1, e2) -> {
            return Math.abs(e1.distance - e2.distance);
        });

        if (distance <= TOLERANCE) {
            words.add(new Element(root.word, distance));
        }

        IntStream.rangeClosed(minRange(distance - TOLERANCE), distance + TOLERANCE).forEach(diff -> {
            Node ptr = root.adjacentNodes.get(diff);

            if (ptr != null) {
                words.addAll(getWordsWithinRange(ptr, missSpelled));
            }
        });

        return words;
    }

    private static List<String> getClosestWords(String missSpelled) {

        Node root = formBkTree(missSpelled);
        PriorityQueue<Element> queue = getWordsWithinRange(root, missSpelled);

        return queue.stream().map(x -> x.word).collect(Collectors.toList());
    }

    public static void main(String [] args) throws IOException {
        //int editDistance = editDistance("EXECUTION", "INTENTION");

       List<String> words = Files.readAllLines(Paths.get("/home/guruprasad/corncob_caps.txt"));
        wordLengthMap = group(words);

        List<String> similarWords = getClosestWords("INDFER");

        similarWords.stream().forEach(System.out::println);

        /*int editDistance = editDistance("REMIND", "FREIND");
        System.out.println(editDistance);*/
    }

}
