package spellCheck;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EditDistance {


    public static int editDistance(String str1, String str2) {
        str1 = str1.trim().toUpperCase();
        str2 = str2.trim().toUpperCase();

        char[] word1 = str1.toCharArray();
        char[] word2 = str2.toCharArray();

        int[][] matrix = new int[str1.length() + 1][str2.length() + 1];

        for (int i = 0; i <= str1.length(); i++) {
            matrix[i][0] = i;
        }

        for (int j = 0; j <= str2.length(); j++) {
            matrix[0][j] = j;
        }

        for (int i = 1; i <= str1.length(); i++) {
            for (int j = 1; j <= str2.length(); j++) {

                int factor = 0;
                if (word1[i - 1] != word2[j - 1]) {
                    factor = 2;
                }
                matrix[i][j] = min(matrix[i - 1][j] + 1, matrix[i][j - 1] + 1, matrix[i - 1][j - 1] + factor);
            }
        }


        return matrix[str1.length()][str2.length()];
    }

    private static int min(int d1, int d2, int d3) {
        int min = d1;

        if (d2 < min)
            min = d2;

        if (d3 < min)
            min = d3;

        return min;

    }

    public static void main (String [] args) {
        int d = editDistance("CAT", "BAT");
        System.out.println(d);

    }

}

