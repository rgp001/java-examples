package tsp;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TSP {

    static int[][] adjacencyMatrix = new int[][]{
            {0, 10, 15, 20},
            {10, 0, 35, 25},
            {15, 35, 0, 30},
            {20, 25, 30, 0}
    };

    static class Key {

        int startIndex;
        Set<Integer> citiesToTraverse;


        public Key(int startIndex, Set<Integer> citiesToTraverse) {
            this.startIndex = startIndex;
            this.citiesToTraverse = citiesToTraverse;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Key key = (Key) o;
            return startIndex == key.startIndex &&
                    Objects.equals(citiesToTraverse, key.citiesToTraverse);
        }

        @Override
        public int hashCode() {
            return Objects.hash(startIndex, citiesToTraverse);
        }
    }

    static class Value {
        int cost;

        public Value(int cost, int index) {
            this.cost = cost;
            this.index = index;
        }

        int index;

    }

    static Map<Key, Value> citiesTraversalCost = new HashMap<>();

    static Value calculateMinDistance() {

        int nCities = adjacencyMatrix.length;
        Set<Integer> cities = IntStream.rangeClosed(2, nCities).boxed().collect(Collectors.toSet());

        Value val = calculateMinDistance(1, cities);

        citiesTraversalCost.put(new Key(1, cities), val);

        return val;
    }


    static Value calculateMinDistance(int index, Set<Integer> remainingCities) {

        if (remainingCities.isEmpty()) {

            Value val = new Value(adjacencyMatrix[index - 1][0], 0);
            citiesTraversalCost.put(new Key(index, Set.of()), val);
            return val;
        } else {

            Key key = new Key(index, remainingCities);

            if (citiesTraversalCost.containsKey(key)) {
                /*System.out.println("\nYes dp works ");
                System.out.println("Start city " + index);

                remainingCities.stream().forEach( i -> System.out.print(" " + i));
*/
                return citiesTraversalCost.get(key);
            }

            final AtomicInteger min = new AtomicInteger(Integer.MAX_VALUE);
            final AtomicInteger nextIndex = new AtomicInteger(0);


            remainingCities.stream().forEach(i -> {

                Set<Integer> subSet = remainingCities.stream().filter(j -> j != i).collect(Collectors.toSet());
                int temp = calculateMinDistance(i, subSet).cost;

                if (adjacencyMatrix[index - 1][i - 1] + temp < min.get()) {
                    min.set(adjacencyMatrix[index - 1][i - 1] + temp);
                    nextIndex.set(i);
                }

            });

            Value value = new Value(min.get(), nextIndex.get());
            citiesTraversalCost.put(new Key(index, remainingCities), value);
            return value;

        }
    }

    static void printPath() {

        Value minCostPath = calculateMinDistance();

        System.out.println("Min distance is " + minCostPath.cost);

        int nCities = adjacencyMatrix.length;
        Set<Integer> cities = IntStream.rangeClosed(2, nCities).boxed().collect(Collectors.toSet());

        List<Integer> path = new ArrayList<>();
        path.add(1);

        int next = minCostPath.index;

        while (next != 0) {
            path.add(next);
            cities.remove(next);
            Value val = citiesTraversalCost.get(new Key(next, cities));
            next = val.index;
        }

        path.add(1);

        path.stream().forEach( i -> System.out.print(" " + i));

    }

    public static void main(String[] args) {

        printPath();

    }
}
