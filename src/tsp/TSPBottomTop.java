package tsp;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class TSPBottomTop {

    static int[][] adjacencyMatrix = new int[][]{
            {0, 12, 29, 22, 13, 24},
            {12, 0, 19, 3, 25, 6},
            {29, 19, 0, 21, 23, 28},
            {22, 3, 21, 0, 4, 5},
            {13, 25, 23, 4, 0, 16},
            {24, 6, 28, 5, 16, 0}
    };


    /*static int[][] adjacencyMatrix = new int[][]{
            {0, 10, 5},
            {10,0,7},
            {5,7,0}
    };*/

    /*static int[][] adjacencyMatrix = new int[][]{
            {0,12,14,17},
            {12,0,15,18},
            {14,15,0,29},
            {17,18,29,0}
    };*/

    static class Key {

        int startIndex;
        Set<Integer> citiesToTraverse;


        public Key(int startIndex, Set<Integer> citiesToTraverse) {
            this.startIndex = startIndex;
            this.citiesToTraverse = citiesToTraverse;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Key key = (Key) o;
            return startIndex == key.startIndex &&
                    Objects.equals(citiesToTraverse, key.citiesToTraverse);
        }

        @Override
        public int hashCode() {
            return Objects.hash(startIndex, citiesToTraverse);
        }
    }

    static class Value {
        int cost;

        public Value(int cost, int index) {
            this.cost = cost;
            this.index = index;
        }

        int index;

    }

    static Map<Key, Value> citiesTraversalCost = new HashMap<>();


    static Value calculateMinDistance() {

        int nCities = adjacencyMatrix.length;
        Set<Integer> cities = IntStream.rangeClosed(2, nCities).boxed().collect(Collectors.toSet());

        Value val = calculateMinDistance(1, cities);

        citiesTraversalCost.put(new Key(1, Set.of()), new Value(0,0));

        return val;
    }


    static Value calculateMinDistance(int index, Set<Integer> allCities) {

        Queue<Key> queue = new LinkedList<>();
        Set<Integer> remainingCities = new HashSet(allCities);

        Key superKey = new Key(index, allCities);

        do {
            if (queue.isEmpty()) {
                for (int city : remainingCities) {
                    Key key = new Key(city, Set.of());
                    Value value = new Value(adjacencyMatrix[city - 1][0], 1);
                    citiesTraversalCost.put(key, value);
                    queue.add(key);
                }
            } else {

                Key keyVisited = queue.remove();

                Set<Integer> visitedCities = new HashSet<>();

                visitedCities.addAll(keyVisited.citiesToTraverse);
                visitedCities.add(keyVisited.startIndex);

                remainingCities = new HashSet(allCities);
                remainingCities.removeAll(visitedCities);

                if (remainingCities.isEmpty()) {

                    int cost = adjacencyMatrix[0][keyVisited.startIndex-1] + citiesTraversalCost.get(keyVisited).cost;

                    Optional.ofNullable(citiesTraversalCost.get(superKey)).ifPresentOrElse(value -> {
                        if (value.cost > cost) {
                            citiesTraversalCost.put(superKey, new Value(cost, keyVisited.startIndex));
                        }
                    }, () ->{
                        citiesTraversalCost.put(superKey, new Value(cost, keyVisited.startIndex));
                    });

                } else {
                    for (int city : remainingCities) {
                        Key key = new Key(city, visitedCities);
                        int cost = adjacencyMatrix[city-1][keyVisited.startIndex-1] + citiesTraversalCost.get(keyVisited).cost;

                        Optional.ofNullable(citiesTraversalCost.get(key)).ifPresentOrElse(value -> {
                            if (value.cost > cost) {
                                citiesTraversalCost.put(key, new Value(cost, keyVisited.startIndex));
                            }
                        }, () ->{
                            queue.add(key);
                            citiesTraversalCost.put(key, new Value(cost, keyVisited.startIndex));
                        });

                    }
                }

            }

        } while (!queue.isEmpty());

        return citiesTraversalCost.get(superKey);
    }

    static void printPath() {

        Value minCostPath = calculateMinDistance();

        System.out.println("Min distance is " + minCostPath.cost);

        int nCities = adjacencyMatrix.length;
        Set<Integer> cities = IntStream.rangeClosed(2, nCities).boxed().collect(Collectors.toSet());

        List<Integer> path = new ArrayList<>();
        path.add(1);

        int next = minCostPath.index;

        while (next != 0) {
            path.add(next);
            cities.remove(next);
            Value val = citiesTraversalCost.get(new Key(next, cities));
            next = val.index;
        }

        path.stream().forEach( i -> System.out.print(" " + i));

    }

    public static void main(String[] args) {

        printPath();

    }
}
