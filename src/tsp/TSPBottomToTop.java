package tsp;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TSPBottomToTop {

    // row is the source city and col is the destination city.
    /*static int[][] adjacencyMatrix = new int[][]{
            {0, 10, 15, 20},
            {10, 0, 35, 25},
            {15, 35, 0, 30},
            {20, 25, 30, 0}
    };*/

    /*static int[][] adjacencyMatrix = new int[][]{
            {0,12,14,17},
            {12,0,15,18},
            {14,15,0,29},
            {17,18,29,0}
    };*/

    static int[][] adjacencyMatrix = new int[][]{
            {0, 12, 29, 22, 13, 24},
            {12, 0, 19, 3, 25, 6},
            {29, 19, 0, 21, 23, 28},
            {22, 3, 21, 0, 4, 5},
            {13, 25, 23, 4, 0, 16},
            {24, 6, 28, 5, 16, 0}
    };

   record Key(int startCity,  Set<Integer> citiesToTraverse){}

    /*static class Key {

        int startCity;
        Set<Integer> citiesToTraverse;


        public Key(int startCity, Set<Integer> citiesToTraverse) {
            this.startCity = startCity;
            this.citiesToTraverse = citiesToTraverse;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TSPBottomToTop.Key key = (TSPBottomToTop.Key) o;
            return startCity == key.startCity &&
                    Objects.equals(citiesToTraverse, key.citiesToTraverse);
        }

        @Override
        public int hashCode() {
            return Objects.hash(startCity, citiesToTraverse);
        }
    }*/

    static void calculateMinDistance() {
        int nCities = adjacencyMatrix.length;
        Set<Integer> cities = IntStream.rangeClosed(2, nCities).boxed().collect(Collectors.toSet());
        Map<Key, Integer> table = new HashMap<>();
        Queue<Key> queue = new LinkedList<>();
        LinkedList<Integer> path = new LinkedList<>();
        path.addFirst(1);

        cities.stream().forEach(city -> {
            Key key = new Key(city, Set.of());
            table.put(key, adjacencyMatrix[city-1][0]);
            queue.add(key);
        });

        do {
            Key key = queue.remove();
            Set<Integer> remainingCities = new HashSet<>(cities);
            remainingCities.remove(key.startCity);
            remainingCities.removeAll(key.citiesToTraverse);

            remainingCities.stream().forEach(remainingCity -> {
                //int min = adjacencyMatrix[key.startCity - 1][city - 1] + table.get(new Key(city, key.citiesToTraverse));
                int min = Integer.MAX_VALUE;
                Set<Integer> temp = new HashSet<>(key.citiesToTraverse);
                temp.add(remainingCity);

                for (int city : temp) {
                    Set<Integer> subCities = temp.stream().filter(sc -> sc != city).collect(Collectors.toSet());
                    int cost = adjacencyMatrix[key.startCity - 1][city - 1] + table.get(new Key(city, subCities));
                    min = cost < min ? cost : min;
                }
                Key k = new Key(key.startCity, temp);
                table.put(k, min);
                queue.add(k);

            });
        } while(!queue.isEmpty());

        var topLevelCities = table.entrySet().stream().filter(entry -> entry.getKey().citiesToTraverse.size() == adjacencyMatrix.length -2).collect(Collectors.toList());

        int min = Integer.MAX_VALUE;
        int startCity = 0;
        for (var topLevel : topLevelCities) {
            int cost = adjacencyMatrix[0][topLevel.getKey().startCity-1] + topLevel.getValue();

            if (cost < min) {
                min = cost;
                startCity = topLevel.getKey().startCity;
            }
        }
        path.addLast(startCity);
        List<String> display = path.stream().map(Object::toString).collect(Collectors.toList());
        System.out.println("Minimum cost " + min);
        System.out.println("Path " + String.join(" ", display));
    }

    public static void main(String [] args) {
        BitSet bitSet = new BitSet();

        //bitSet.set(10);

        System.out.println(bitSet.size());
        calculateMinDistance();
    }
}
