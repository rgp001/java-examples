import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamsDemo {

    private static void oddEvenSegregation(int n) {

        List<Integer> even = new ArrayList<>();
        List<Integer> odd = new ArrayList<>();


        IntStream stream = IntStream.range(1, n+1);

        boolean yes = stream.anyMatch((x) -> x > 20);


        /*stream.parallel().forEachOrdered((x) -> {
            if ((x % 2 == 0)) {
                even.add(x);
            } else {
                odd.add(x);
            }
        });*/

        stream.forEach((int x) -> {
            if ((x % 2 == 0)) {
                even.add(x);
            } else {
                odd.add(x);
            }
        });

        even.stream().forEach(System.out::print);
        System.out.println();
        odd.stream().forEach(System.out::print);

        /*IntStream evenStream = stream.filter((x) -> x % 2 == 0);

        IntStream oddStream = stream.filter((x) -> x % 2 == 1);
        evenStream.forEach(System.out::print);
        System.out.println("");
        oddStream.forEach(System.out::print);*/


    }

    public static void main (String [] args) {

        oddEvenSegregation(10);

        /*String[] arr = new String[]{"a", "b", "c"};
        Stream<String> stream = Arrays.stream(arr);
        stream = Stream.of("a", "b", "c");

        stream.forEach(System.out::append);
        //stream.forEach(System.out::append);*/
    }
}
