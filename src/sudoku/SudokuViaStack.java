package sudoku;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;

public class SudokuViaStack extends Sudoku {


    static class Tuple {

        int k;
        int potentialNum;

        public Tuple(int k, int potentailNum) {
            this.k = k;
            this.potentialNum = potentailNum;
        }
    }


    protected void solve() {

        Stack<Tuple> stack = new Stack<>();

        int k = 0;


        while (k < 81) {

            int row = k / 9;
            int column = k % 9;

            if (grid[row][column] != 0) {
                k++;
            } else {

                if (!stack.empty()) {
                    Tuple tuple = stack.peek();

                   /* if (tuple.k == k) {
                        k++;
                        grid[row][column] = 0;
                    } else if (tuple.k < k) {
                        grid[row][column] = 0;
                        k = tuple.k +1;
                    }else {*/

                        int prevrow = tuple.k / 9;
                        int prevcol = tuple.k % 9;

                        int num = tuple.potentialNum;

                        int prevk = tuple.k;

                        grid[prevrow][prevcol] = num;

                        Set<Integer> poolOfFreeNumbers = getFreePool(row, column);

                        if (poolOfFreeNumbers.isEmpty()) {

                            // keep popping till next k is not equal to prev k

                            grid[prevrow][prevcol] = 0;


                            tuple = stack.pop();

                            while (!stack.empty() && prevk != stack.peek().k) {

                                tuple = stack.pop();

                                prevk = tuple.k;
                                prevrow = tuple.k / 9;
                                prevcol = tuple.k % 9;
                                grid[prevrow][prevcol] = 0;
                            }

                            k = prevk + 1;

                        } else {

                            for (Integer number : poolOfFreeNumbers) {

                                Tuple tup = new Tuple(k, number);
                                stack.push(tup);
                            }
                            k++;

                        }
                   // }

                } else {

                    Set<Integer> poolOfFreeNumbers = getFreePool(row, column);
                    for (Integer number : poolOfFreeNumbers) {

                        Tuple tup = new Tuple(k, number);
                        stack.push(tup);
                    }
                    k++;

                }


            }
        }

        if (!stack.isEmpty()) {
            Tuple tuple = stack.pop();
            int prevrow = tuple.k / 9;
            int prevcol = tuple.k % 9;

            grid[prevrow][prevcol] = tuple.potentialNum;
        }

    }

    public  static void main (String [] args) throws FileNotFoundException {
        SudokuViaStack sudoku = new SudokuViaStack();

        {

            Scanner sc = new Scanner(new BufferedReader(new FileReader("/home/guruprasad/sample.txt")));



            while (sc.hasNextLine()) {
                for (int i = 0; i < sudoku.grid.length; i++) {
                    String[] line = sc.nextLine().trim().split(" ");
                    for (int j = 0; j < sudoku.grid.length; j++) {
                        sudoku.grid[i][j] = Integer.parseInt(line[j]);
                    }
                }

            }

            sc.close();

            sudoku.printGrid();

            System.out.println(" Change effective --------------------------------------");
            long t1 = System.currentTimeMillis();
            sudoku.solve();
            long t2 = System.currentTimeMillis();
            System.out.println("--------------------------------------" + (t2-t1));
            sudoku.printGrid();
            //getNumbersInColumn(0).stream().forEach(System.out::print);
        }
    }


}
