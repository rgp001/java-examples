package sudoku;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Sudoku {

	protected  int[][] grid = new int[9][9];
	
	protected static Set<Integer> ONE_TO_NINE = IntStream.rangeClosed(1, 9).boxed().collect(Collectors.toSet());


	protected  boolean solve(int k) {
		
		int row = k/9;
		int column = k%9;
		
		if (k == 81) {
			return true;
		} else if (grid[row][column] !=0 ) {
			return solve(k+1);
		} else {
			
			Set<Integer> poolOfFreeNumbers = getFreePool(row, column);
			
			if (poolOfFreeNumbers.isEmpty())
				return false;
			
			for (Integer number : poolOfFreeNumbers) {
				grid[row][column] = number;
				
				if (solve(k+1)) {
					return true;
				} 
			}
			
			grid[row][column] = 0;
			return false;
		}
		//return false;
	}


	protected  Set<Integer> getFreePool(int row, int column) {
		
		Set<Integer> numbersInRow = getNumbersInRow(row);
		Set<Integer> numbersInColumn = getNumbersInColumn(column);
		Set<Integer> numbersInBlock = getNumbersInBlock(row, column);
		
		
		Set<Integer> free = new HashSet<Integer>(ONE_TO_NINE);
		
		free.removeAll(numbersInRow);
		free.removeAll(numbersInColumn);
		free.removeAll(numbersInBlock);
		
		return free;
	}


	protected  Set<Integer> getNumbersInBlock(int row, int column) {
		
		int startRow = (row /3)*3;
		int endRow = startRow + 2;
		
		int startColumn = (column/3)*3;
		int endColumn = startColumn + 2;
		
		Set<Integer> pool = new HashSet<Integer>();
		
		
		for (int i = startRow;  i <= endRow; i++) {
			
			for (int j = startColumn; j <= endColumn; j++) {
				if (grid[i][j] != 0) {
					pool.add(grid[i][j]);
				}
			}
		}
		
		return pool;
	}


	protected Set<Integer> getNumbersInColumn(int column) {
		
		
		Set<Integer> columnValues = new LinkedHashSet<>();
		
		for (int i = 0; i < 9; i++) {
			if (grid[i][column] != 0) {
				columnValues.add(grid[i][column]);
			}
			
		}
		
		return columnValues;
	}


	protected  Set<Integer> getNumbersInRow(int row) {
		
		return Arrays.stream(grid[row]).boxed().filter(x -> x!=0).collect(Collectors.toCollection(LinkedHashSet::new));
	}


	protected  void solve() {
		solve(0);
	}

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new BufferedReader(new FileReader("/home/guruprasad/sample.txt")));

		Sudoku sudoku = new Sudoku();

		while (sc.hasNextLine()) {
			for (int i = 0; i < sudoku.grid.length; i++) {
				String[] line = sc.nextLine().trim().split(" ");
				for (int j = 0; j < sudoku.grid.length; j++) {
					sudoku.grid[i][j] = Integer.parseInt(line[j]);
				}
			}

		}
		
		sc.close();
		
		sudoku.printGrid();
		
		System.out.println("--------------------------------------");
		long t1 = System.currentTimeMillis();
		sudoku.solve();
		long t2 = System.currentTimeMillis();
		System.out.println("--------------------------------------" + (t2-t1));
		sudoku.printGrid();
		//getNumbersInColumn(0).stream().forEach(System.out::print);
	}
	
	protected  void printGrid() {
		
		Arrays.stream(grid).forEach(arr -> {
			System.out.println(Arrays.stream(arr).mapToObj(i -> String.valueOf(i)).reduce((x,y) -> x + " " + y).get());
		});
		
		
	}
	
	
}
