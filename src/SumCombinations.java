import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.lang.Integer.min;

public class SumCombinations {


    private static List<int[]> combine(int n) {

        int [] arr = new int[n];
        Map<Integer, List<int[]>> memory = new HashMap<>();
        return combine(0, arr, n);
    }

    private static List<int[]> combine(int startIndex, int [] arr, int remainingNumber) {

        List<int[]> result = new LinkedList<>();

        if (remainingNumber == 0) {

            int [] temp = Arrays.copyOf(arr, arr.length);
            result.add(temp);

            Arrays.fill(arr, startIndex-1, arr.length-1, 0);
            return result;
        }

        int maxNumber = startIndex == 0 ? remainingNumber : min(remainingNumber, arr[startIndex -1]);


        for (int i = maxNumber; i >= 1; i-- ) {
            Arrays.fill(arr, startIndex, arr.length-1, 0);
            arr[startIndex] = i;
            result.addAll(combine(startIndex+1, arr, remainingNumber - i));
        }

        return result;
    }



    public static void main (String [] args) {

        int n = 9;
        List<int[]> results = combine(n);

        results.stream().forEach(arr -> {
            System.out.print(Arrays.stream(arr).filter(x -> x > 0).mapToObj(String::valueOf).reduce((x,y) -> x + "+" + y).get());
            System.out.println();
        });

    }

}
