import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GreedyFiller {


    private static int[][] fill(int n) {

        int [][] arr = new int[n][n];

        for (int i = 0; i < n; i++) {
            Arrays.fill(arr[i], 0);
        }

        BitSet used = new BitSet(n*n);


        canFill(arr, n, 0, used, sumInaRow(n), 0);

        return arr;
    }

    private static boolean canFill(int[][] arr, int dimension, int index, BitSet used, int sumInAnyRow, int bitPosition) {



        int row = index/dimension;
        int column = index % dimension;

        if (column == 0) {
            bitPosition = 0;
        }


        if (bitPosition >= dimension * dimension) return false;

       // System.out.println(" Row " + row + " Column " + column);

        //Begining of a new row look greedily

        bitPosition = used.nextClearBit(bitPosition);

        int prospect = dimension*dimension - used.nextClearBit(bitPosition);

        if (prospect == 0 ) {
            return false;
            //System.out.println(bitPosition + " Row " + row + " Column " + column);
        }

        if (Arrays.stream(arr[row]).sum() + prospect <= sumInAnyRow) {
            used.set(bitPosition);
            arr[row][column] = prospect;

            if (index == dimension*dimension -1 )
                return true;

            if (!canFill(arr, dimension, index+1, used, sumInAnyRow, bitPosition +1)) {
                used.set(bitPosition, false);
                arr[row][column] = 0;

                if (bitPosition >= dimension*dimension) return false;

                return canFill(arr, dimension, index, used, sumInAnyRow, bitPosition+1);
            }
        } else {
            return canFill(arr, dimension, index, used, sumInAnyRow, bitPosition+1);
        }

        return true;
    }

    private static int sumInaRow(int dimension) {

        return ((1 + (dimension*dimension))*dimension)/2;
    }


    private static void print(int[][] arr) {

        Arrays.stream(arr).map((singlearr) -> {
            return Arrays.stream(singlearr).mapToObj(Integer::toString).reduce((x, y) -> x + " " + y).get();
        }).forEach(System.out::println);
    }


    public static void main (String [] args) {


        int [][] arr = fill(6);

        print(arr);


    }
}
