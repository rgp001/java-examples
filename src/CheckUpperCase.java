import java.util.function.IntPredicate;

public class CheckUpperCase {

    public static void main(String[] args) {

        String test = "abCdEbc";
        boolean hasUpper = test.chars().anyMatch(ch -> Character.isUpperCase(ch));
        System.out.print(hasUpper + "\n");

        test.chars().mapToObj(i -> (char)i).distinct().forEach(System.out::print);
    }
}
