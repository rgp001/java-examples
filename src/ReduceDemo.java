import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ReduceDemo {
    public static void main(String[] args) {
        int[] array = {2,3,4,5};
        IntStream st = Arrays.stream(array);
        System.out.println(st.reduce(10,(x, y) -> x*y));
        //Arrays.stream(array).reduce(Integer::sum).ifPresent(s -> System.out.println(s));

        String[] myArray = { "this", "is", "a", "sentence" };
        String result = Arrays.stream(myArray)
                .reduce( (a,b) -> a + " " + b).get();

        System.out.print(result);

    }
}